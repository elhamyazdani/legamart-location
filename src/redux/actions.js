import { createAction } from '@reduxjs/toolkit'
import { DEFAULT_EDITING_LOCATION } from '../constants'

export const saveLocationAction = createAction('SAVE_LOCATION')
export const changePageStatueAction = createAction('PAGE_STATUS')
export const setEditingLocationAction = createAction('EDITING_LOCATION')

export const saveLocation = location => dispatch =>
  dispatch({ type: 'SAVE_LOCATION', payload: location })

export const changePageStatue = status => dispatch =>
  dispatch({ type: 'PAGE_STATUS', payload: status })

export const setEditingLocation = location => dispatch =>
  dispatch({ type: 'EDITING_LOCATION', payload: location })

export const resetEditingLocation = () => dispatch =>
  dispatch({ type: 'EDITING_LOCATION', payload: DEFAULT_EDITING_LOCATION })
