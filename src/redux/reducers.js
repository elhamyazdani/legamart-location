import { combineReducers, createReducer } from '@reduxjs/toolkit'
import { v1 as generateId } from 'uuid'
import { DEFAULT_EDITING_LOCATION, DEFAULT_PAGE } from '../constants'
import {
  saveLocationAction,
  changePageStatueAction,
  setEditingLocationAction
} from './actions'

const locations = createReducer([], {
  [saveLocationAction]: (state, action) => {
    const location = action.payload
    if (location.id) {
      const newLocations = state.filter(l => l.id !== location.id)
      return [...newLocations, location]
    } else {
      return [...state, { ...location, id: generateId() }]
    }
  }
})

const pageStatus = createReducer(DEFAULT_PAGE, {
  [changePageStatueAction]: (state, action) => action.payload
})

const editingLocation = createReducer(DEFAULT_EDITING_LOCATION, {
  [setEditingLocationAction]: (state, action) => action.payload
})

export default combineReducers({
  locations,
  pageStatus,
  editingLocation
})
