export const MAP_CENTER = {
  lat: 51.5072,
  lng: 0.1276
}

export const DEFAULT_PAGE = 'SHOW_LOCATIONS' // or ADD_LOCATION

export const DEFAULT_EDITING_LOCATION = {
  title: '',
  position: MAP_CENTER,
  type: undefined,
  logo: undefined
}


export const TYPE_OPTIONS = [
  { value: 'business', label: 'Business' },
  { value: 'home', label: 'Home' },
  { value: 'school', label: 'School' }
]
