import { MapContainer, TileLayer } from 'react-leaflet'
import '../../assets/css/components/MapField.css'
import { MAP_CENTER } from '../../constants'
import DraggableMarker from './DraggableMarker'

export default function MapField ({ label, value, setValue }) {
  return (
    <div className='form-item map-field'>
      <div className='label'>{label}</div>
      <div className='field'>
        <MapContainer center={MAP_CENTER} zoom={13} scrollWheelZoom={false}>
          <TileLayer
            attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
            url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
          />
          <DraggableMarker value={value} setValue={setValue} />
        </MapContainer>
      </div>
    </div>
  )
}
