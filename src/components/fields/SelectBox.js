import Select from 'react-select'
import { TYPE_OPTIONS } from '../../constants'

export default function SelectBox ({ label, value, setValue }) {
  return (
    <div className='form-item select-box'>
      <div className='label'>{label}</div>
      <div className='field'>
        <Select
          defaultValue={value}
          onChange={value => setValue(value)}
          options={TYPE_OPTIONS}
          isSearchable={false}
        />
      </div>
    </div>
  )
}
