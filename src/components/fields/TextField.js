export default function TextField ({ label, value, setValue }) {
  return (
    <div className='form-item text-field'>
      <div className='label'>{label}</div>
      <div className='field'>
        <input
          type='text'
          value={value}
          onChange={e => setValue(e.target.value)}
        />
      </div>
    </div>
  )
}
