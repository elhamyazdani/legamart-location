import Dropzone from 'react-dropzone'
import '../../assets/css/components/Uploader.css'
import upload from '../../assets/images/upload.png'

export default function Uploader ({ label, value, setValue }) {
  function fileSelectedHandler (acceptedFiles) {
    setValue({
      url: URL.createObjectURL(acceptedFiles[0]),
      path: acceptedFiles[0].path,
      name: acceptedFiles[0].name
    })
  }

  function handleClose () {
    setValue()
  }

  return (
    <div className='form-item uploader'>
      <div className='label'>{label}</div>
      <div className='field'>
        <Dropzone onDrop={acceptedFiles => fileSelectedHandler(acceptedFiles)}>
          {({ getRootProps, getInputProps }) => (
            <div className='box'>
              {value ? (
                <div className='uploaded-zone'>
                  <div className='image'>
                    <img src={value.url} alt={value.name} id='thumbnail' />
                  </div>
                  <div className='name'>{value.name}</div>
                  <div className='close' onClick={handleClose}>
                    X
                  </div>
                </div>
              ) : (
                <div className='drop-zone' {...getRootProps()}>
                  <input {...getInputProps()} />
                  <img src={upload} alt='upload' />
                  <p>
                    Drop the image here <br />
                    or click to select file
                  </p>
                </div>
              )}
            </div>
          )}
        </Dropzone>
      </div>
    </div>
  )
}
