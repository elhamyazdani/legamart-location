import { useMemo, useRef } from 'react'
import { Marker } from 'react-leaflet'

export default function DraggableMarker({value, setValue }) {
  const markerRef = useRef(null)

  const eventHandlers = useMemo(
    () => ({
      dragend () {
        const marker = markerRef.current
        if (marker != null) {
          setValue({ ...marker.getLatLng() })
        }
      }
    }),
    [setValue]
  )

  return (
    <Marker
      draggable={true}
      eventHandlers={eventHandlers}
      position={value}
      ref={markerRef}
    />
  )
}