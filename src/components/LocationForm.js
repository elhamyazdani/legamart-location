import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import '../assets/css/components/LocationForm.css'
import '../assets/css/generals/form.css'
import {
  saveLocation,
  changePageStatue,
  resetEditingLocation
} from '../redux/actions'
import MapField from './fields/MapField'
import SelectBox from './fields/SelectBox'
import TextField from './fields/TextField'
import Uploader from './fields/Uploader'
import Button from './utils/Button'

export default function LocationForm () {
  const editingLocation = useSelector(s => s.editingLocation)

  const [title, setTitle] = useState(editingLocation.title)
  const [position, setPosition] = useState(editingLocation.position)
  const [type, setType] = useState(editingLocation.type)
  const [logo, setLogo] = useState(editingLocation.logo)

  const dispatch = useDispatch()

  function handleSubmit (e) {
    e.preventDefault()

    if (title && type && position && logo) {
      dispatch(
        saveLocation({ id: editingLocation.id, title, type, position, logo })
      )
      dispatch(changePageStatue('SHOW_LOCATIONS'))
    }
  }

  function handleCancel () {
    dispatch(resetEditingLocation)
    dispatch(changePageStatue('SHOW_LOCATIONS'))
  }

  return (
    <div>
      <form className='location-form' onSubmit={handleSubmit}>
        <div className='body'>
          <TextField label='Location name' value={title} setValue={setTitle} />
          <MapField
            label='Location on map'
            value={position}
            setValue={setPosition}
          />
          <SelectBox label='Location type' value={type} setValue={setType} />
          <Uploader label='Logo' value={logo} setValue={setLogo} />
          <div className='form-item form-actions'>
            <Button type='button' onClick={handleCancel}>
              Cancel
            </Button>
            <Button type='submit' className='primary'>
              Save
            </Button>
          </div>
        </div>
      </form>
    </div>
  )
}
