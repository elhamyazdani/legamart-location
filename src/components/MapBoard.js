import { useState } from 'react'
import { MapContainer, Marker, Popup, TileLayer } from 'react-leaflet'
import { useDispatch, useSelector } from 'react-redux'
import '../assets/css/components/MapBoard.css'
import { MAP_CENTER } from '../constants'
import { changePageStatue, setEditingLocation } from '../redux/actions'
import Button from './utils/Button'

export default function MapBoard () {
  const [activeLocation, setActiveLocation] = useState(null)
  const locations = useSelector(s => s.locations)
  const dispatch = useDispatch()

  function handleEdit (e, location) {
    e.stopPropagation()
    dispatch(setEditingLocation(location))
    dispatch(changePageStatue('ADD_LOCATION'))
  }

  return ( 
    <div className='map-board'>
      <MapContainer center={MAP_CENTER} zoom={12} scrollWheelZoom={true}>
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
          url='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        />
        {locations.map(location => (
          <Marker
            key={location.id}
            position={[location.position.lat, location.position.lng]}
            eventHandlers={{
              click: () => {
                setActiveLocation(location)
              }
            }}
          />
        ))}

        {activeLocation && (
          <Popup
            position={activeLocation.position}
            onClose={() => {
              setActiveLocation(null)
            }}
          >
            <div className='popup'>
              <div className='title'>Location Details</div>
              <div className='body'>
                <div className='logo'>
                  <img src={activeLocation.logo.url} alt='' />
                </div>
                <div className='name'>{activeLocation.title}</div>
                <div className='type'>{activeLocation.type.label}</div>
                <div className='actions'>
                  <Button onClick={e => handleEdit(e, activeLocation)}>
                    Edit
                  </Button>
                </div>
              </div>
            </div>
          </Popup>
        )}
      </MapContainer>
    </div>
  )
}
