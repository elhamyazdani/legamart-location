import { useDispatch, useSelector } from 'react-redux'
import '../assets/css/components/Header.css'
import { changePageStatue } from '../redux/actions'
import Button from './utils/Button'

export default function Header () {
  const pageStatus = useSelector(state => state.pageStatus)
  const dispatch = useDispatch()

  function changeStatus (status) {
    dispatch(changePageStatue(status))
  }

  return (
    <header>
      <div className='header-inner'>
        <div className='title'>
          {pageStatus === 'SHOW_LOCATIONS' ? (
            <h1>Shared Locations</h1>
          ) : (
            <h1>Share your location</h1>
          )}
        </div>
        <div className='add-location-links'>
          {pageStatus === 'SHOW_LOCATIONS' ? (
            <Button
              className='primary'
              onClick={() => changeStatus('ADD_LOCATION')}
            >
              Share your location
            </Button>
          ) : (
            <Button
              className='secondary'
              onClick={() => changeStatus('SHOW_LOCATIONS')}
            >
              Back to list
            </Button>
          )}
        </div>
      </div>
    </header>
  )
}
