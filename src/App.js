import LocationForm from './components/LocationForm'
import MapBoard from './components/MapBoard'
import './assets/css/components/App.css'
import Header from './components/Header'
import { useSelector } from 'react-redux'

export default function App () {
  const pageStatus = useSelector(state => state.pageStatus)

  return (
    <div className='App'>
      <section className='main-container'>
        <Header />
        <div className='content'>
          {pageStatus === 'SHOW_LOCATIONS' ? <MapBoard /> : <LocationForm />}
        </div>
      </section>
    </div>
  )
}
